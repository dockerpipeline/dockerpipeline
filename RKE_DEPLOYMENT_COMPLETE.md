RKE_DEPLOYMENT_COMPLETE

``` # PRE_REQ

• This guide is for preparing the node for RKE installation.
• This guide is for ubuntu server 20.04 OS LTS (latest minor version).
• Commands/ packages may differ for other OS/version.

continue with root - DO IT FOR ALL NODES

Steps:
• Ensure NTP services are up and in sync. ‘timedatectl’
• Install resolvconf using `apt install resolvconf`
• Make sure sysctl parameter is set ‘net.bridge.bridge-nf-call-iptables=1’.
To check “sysctl -a | grep net.bridge.bridge-nf-call-iptables=1”
IF NOT :- cat >>/etc/sysctl.d/kubernetes.conf<<EOF
 net.bridge.bridge-nf-call-iptables = 1
 EOF
 sysctl --system
• . Create one user -> "npcidcl2" without password
 useradd npcidcl2
For docker :-
mkdir /app_dev
chown npcidcl2. /app_dev
Install docker using --->> https://docs.docker.com/engine/install/ubuntu/#install-from-a-package
Go to https://download.docker.com/linux/ubuntu/dists/.
download - FOCAL binaries of arm64
SFTP to bastion
place it in /home/npcidcl2/
dpkg -i /home/npcidcl2/*.deb (via root)
• Deb is all the docker files downloaded
vi /etc/docker/daemon.json
{
"log-driver": "json-file",
"log-opts": {
"max-size": "50m",
"max-file": "10"
},
"graph": "/app_dev/docker"
}
• Add RKE automation user into docker group like npcidcl2.
o Ex: usermod -aG docker npcidcl2
• Restart the docker service after adding the daemon.json file
`systemctl restart docker`
• Allow SSH TCPForwarding
o vi /etc/ssh/sshd_config
o Mark ‘AllowTCPForwarding yes’ entry. This allows tunnelling which is
required to bootstrap RKE nodes.
o Restart the SSHD service.
‘systemctl restart sshd.service’
• create file (/etc/sysctl.d/90-kubelet.conf) and add below content:
vm.overcommit_memory=1
vm.panic_on_oom=0
kernel.panic=10
kernel.panic_on_oops=1
• disable swap from vi/etc/fstab
comment that image line
run init6 in each node too
-> 10.88.1.53 repo.npci.org.in
10.88.1.56 git.npci.org.in
also please add those entries to /etc/cloud/templates/hosts.debian.tmpl on all the
nodes

```

ssh into nodes from 10.211.9.11
su - npcidcl2
The path inside node is cd /home/npcidcl2

Install Rke binary rke binaries using wget if not present 

Create an ssh keypair on the host machine
 ssh-keygen -t rsa -b 2048
 ssh-copy-id –i id_rsa_pub npcidcl2@nodeip
now from 10.211.9.11 you should be able to do ssh to all nodes


``` # Install helm, kubectl, rke binaries- only at host( .11 ) {DaemonSets}

HELM:
$ curl -sfL https://get.helm.sh/helm-v3.5.3-linux-amd64.tar.gz -o helm.tgz
$ tar xf helm.tgz
$ mv linux-amd64/helm /usr/local/bin/
$ chmod +x usr/local/bin/helm
$ helm version
KUBECTL:
$ curl -LO https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl
$ chmod +x kubectl
$ mv kubectl /usr/local/bin/
$ kubectl version
RKE:
curl -LO https://github.com/rancher/rke/releases/download/v1.2.6/rke_linux-amd64
$ mv rke_linux-amd64 rke
$ chmod +x rke
$ rke --version

```


``` Cluster.yaml

# If you intended to deploy Kubernetes in an air-gapped environment,
# please consult the documentation on how to configure custom RKE images.
nodes:
- address: 10.211.10.11
 port: "22"
 role:
 - controlplane
 - worker
 - etcd
 user: npcidcl2
- address: 10.211.10.12
 port: "22"
 role:
 - controlplane
 - etcd
 - worker
 user: npcidcl2
- address: 10.211.10.13
 port: "22"
 role:
 - controlplane
 - etcd
 - worker
 user: npcidcl2
- address: 10.211.10.14
 port: "22"
 role:
 - worker
 user: npcidcl2
- address: 10.211.10.15
 port: "22"
 role:
 - controlplane
 - etcd
 - worker
 user: npcidcl2
services:
 etcd:
 snapshot: true
 retention: "24h"
 creation: "120h"
 backup_config: null
 kube-api:
 service_cluster_ip_range: 10.43.0.0/16
 service_node_port_range: "30000-32767"
 pod_security_policy: false
 always_pull_images: false
 secrets_encryption_config:
 enabled: true
 audit_log:
2
 enabled: true
 configuration:
 max_age: 6
 max_backup: 6
 max_size: 110
 path: /var/log/kube-audit/audit-log.json
 format: json
 policy:
 apiVersion: audit.k8s.io/v1 # This is required.
 kind: Policy
 omitStages:
 - "RequestReceived"
 rules:
 # Log pod changes at RequestResponse level
 - level: RequestResponse
 resources:
 - group: ""
 # Resource "pods" doesn't match requests to any subresource of
pods,
 # which is consistent with the RBAC policy.
 resources: ["pods"]
 admission_configuration: null
 event_rate_limit:
 enabled: true
 kube-controller:
 cluster_cidr: 10.42.0.0/16
 service_cluster_ip_range: 10.43.0.0/16
 kubelet:
 cluster_domain: cluster.local
 infra_container_image: ""
 cluster_dns_server: 10.43.0.10
 fail_swap_on: false
 generate_serving_certificate: false
 extra_args:
 max-pods: 1000
 system-reserved: 'cpu=10000m,memory=50Gi,ephemeral-storage=50Gi'
network:
 plugin: canal
 mtu: 1400
authentication:
 strategy: x509
addons: |-
 ---
 apiVersion: networking.k8s.io/v1
 kind: NetworkPolicy
 metadata:
 name: default-allow-all
 spec:
 podSelector: {}
 ingress:
 - {}
3
 egress:
 - {}
 policyTypes:
 - Ingress
 - Egress
 ---
 apiVersion: v1
 kind: ServiceAccount
 metadata:
 name: default
 automountServiceAccountToken: false
addons_include: []
# system_images:
# etcd: rancher/mirrored-coreos-etcd:v3.5.7
# alpine: rancher/rke-tools:v0.1.96
# nginx_proxy: rancher/rke-tools:v0.1.96
# cert_downloader: rancher/rke-tools:v0.1.96
# kubernetes_services_sidecar: rancher/rke-tools:v0.1.96
# kubedns: rancher/mirrored-k8s-dns-kube-dns:1.22.20
# dnsmasq: rancher/mirrored-k8s-dns-dnsmasq-nanny:1.22.20
# kubedns_sidecar: rancher/mirrored-k8s-dns-sidecar:1.22.20
# kubedns_autoscaler: rancher/mirrored-cluster-proportional-autoscaler:1.8.6
# coredns: rancher/mirrored-coredns-coredns:1.10.1
# coredns_autoscaler: rancher/mirrored-cluster-proportional-autoscaler:1.8.6
# nodelocal: rancher/mirrored-k8s-dns-node-cache:1.22.20
# kubernetes: rancher/hyperkube:v1.27.6-rancher1
# flannel: rancher/mirrored-flannel-flannel:v0.21.4
# flannel_cni: rancher/flannel-cni:v0.3.0-rancher8
# calico_node: rancher/mirrored-calico-node:v3.26.1
# calico_cni: rancher/calico-cni:v3.26.1-rancher1
# calico_controllers: rancher/mirrored-calico-kube-controllers:v3.26.1
# calico_ctl: rancher/mirrored-calico-ctl:v3.26.1
# calico_flexvol: rancher/mirrored-calico-pod2daemon-flexvol:v3.26.1
# canal_node: rancher/mirrored-calico-node:v3.26.1
# canal_cni: rancher/calico-cni:v3.26.1-rancher1
# canal_controllers: rancher/mirrored-calico-kube-controllers:v3.26.1
# canal_flannel: rancher/mirrored-flannel-flannel:v0.21.4
# canal_flexvol: rancher/mirrored-calico-pod2daemon-flexvol:v3.26.1
# weave_node: weaveworks/weave-kube:2.8.1
# weave_cni: weaveworks/weave-npc:2.8.1
# pod_infra_container: rancher/mirrored-pause:3.7
# ingress: rancher/nginx-ingress-controller:nginx-1.8.1-rancher1
# ingress_backend: rancher/mirrored-nginx-ingress-controllerdefaultbackend:1.5-rancher1
# ingress_webhook: rancher/mirrored-ingress-nginx-kube-webhookcertgen:v20230312-helm-chart-4.5.2-28-g66a760794
# metrics_server: rancher/mirrored-metrics-server:v0.6.3
# windows_pod_infra_container: rancher/mirrored-pause:3.7
# aci_cni_deploy_container: noiro/cnideploy:5.2.7.1.81c2369
# aci_host_container: noiro/aci-containers-host:5.2.7.1.81c2369
# aci_opflex_container: noiro/opflex:5.2.7.1.81c2369
4
# aci_mcast_container: noiro/opflex:5.2.7.1.81c2369
# aci_ovs_container: noiro/openvswitch:5.2.7.1.81c2369
# aci_controller_container: noiro/aci-containers-controller:5.2.7.1.81c2369
# aci_gbp_server_container: noiro/gbp-server:5.2.7.1.81c2369
# aci_opflex_server_container: noiro/opflex-server:5.2.7.1.81c2369
authorization:
 mode: rbac
 options: {}
ignore_docker_version: false
enable_cri_dockerd: true
private_registries:
- url: repo.npci.org.in:443
 user: ubuntufocal2004
 password: dtcDr7TQBiMdWFIdrY
 is_default: true
cluster_name: "ubc-vajra"
monitoring:
 provider: "metrics-server"
 tolerations:
 - key: "node.kubernetes.io/unreachable"
 operator: "Exists"
 effect: "NoExecute"
 tolerationseconds: 300
 - key: "node.kubernetes.io/not-ready"
 operator: "Exists"
 effect: "NoExecute"
 tolerationseconds: 300
 metrics_server_priority_class_name: ""

 ```


Provision the cluster
./rke up --config cluster.yml

Once this command completed provisioning the cluster, you will
have cluster state file (cluster.rkestate) and kube config file
(kube_config_cluster.yml) in the current directory.

Install Rook

It is deployment of underlying storage
rook.io:- document of distributed storage https://github.com/rook/rook/tree/release-1.13/Documentation
in github inside charts https://github.com/rook/rook/tree/release-1.13/deploy/charts  is there library ,rook-ceph is cdr and rook-ceph-cluster
download helm charts locally and edit values needed if on cluster we cant download
• Add the link of repo from npci

sudo su-
cd charts/
• Check in vi values.yaml for :- repo,tag,clustername,enable servicemonitor, cephcsi image ,monitoring enabled true 

cd rook-ceph 
Export KUBECONFIG-~/performance-cluster/kubeconfig_cluster.yml

helm install --create-namespace -n rook-ceph rook-ceph . -f values.yaml
helm template --create-namespace -n rook-ceph rook-ceph . -f values.yaml

kubectl -n rook-ceph get pods
kubectl -n rook-ceph get all
kubectl -n rook-ceph get rolebinding
kubectl -n rook-ceph get serviceaccount

these are the resources required for rook now main go to rook-ceph cluster

vi values.yaml

operator namespace ,cluster name , eabled toolbox monitoring enabled , image [pointed to internal nexus repo], 

cd /dev/
dm-0,dm-1 are storage harddrive ment to be used by rook for storage

run the command helm install --create-namespace --namespace rook-ceph-cluster --set operatorNamespace=rook-ceph

install prometheus   go to helm repo blah blah

kubectl creacte -f crds/

cd rook-ceph-cluster

each node have two osd ->interacts with physical disk ,cephsplugin .rdbplugin fcor block

kubectl -n rook-ceph exec -it rook-ceph-tools- --bash

ceph osd tree
ceph osd df tree;- show disk by usage 


cat pvc.yml
kubectl get sc

kubectl apply -f pvcyml





